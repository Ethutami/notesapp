import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

import AntDesign from 'react-native-vector-icons/AntDesign';
import IonIcons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import HomeScreen from '../../pages/HomeScreen';
import AddNew from '../../pages/AddNoteScreen';
import ProfileScreen from '../../pages/ProfileScreen';

const TabNavigator = () => {
  const Tab = createMaterialBottomTabNavigator();
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          let iconName2;
          let iconName3;

          if (route.name === 'Home') {
            iconName = focused ? 'appstore1' : 'appstore-o';
          } else if (route.name === 'AddNotes') {
            iconName2 = focused ? 'add-circle' : 'add-circle-outline';
          } else if (route.name === 'Profile') {
            iconName3 = focused ? 'user' : 'user-o';
          }
          // You can return any component that you like here!
          return (
            <View style={{flexDirection: 'row'}}>
              <AntDesign name={iconName} size={size} color={color} />
              <IonIcons name={iconName2} size={size} color={color} />
              <FontAwesome name={iconName3} size={size} color={color} />
            </View>
          );
        },
      })}
      tabBarOptions={{
        activeColor: '#339CD8',
        inactiveColor: 'gray',
      }}
      barStyle={{backgroundColor: '#9AD6F2'}}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{headerShown: false}}
      />
      <Tab.Screen name="AddNotes" component={AddNew} />
      <Tab.Screen name="Profile" component={ProfileScreen} />
      {/* <Tab.Screen name="Profile" component={tesMoment} /> */}
    </Tab.Navigator>
  );
};

export default TabNavigator;

const styles = StyleSheet.create({});
