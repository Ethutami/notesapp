// import React from 'react';
// import {useNavigation} from '@react-navigation/core';
// import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

// const ButtonAdd = () => {
//   const navigation = useNavigation();
//   return (
//     <View>
//       <TouchableOpacity
//         onPress={() => navigation.navigate('NewNotes')}
//         style={styles.addButton}>
//         <Text style={styles.addButtonText}> + </Text>
//       </TouchableOpacity>
//     </View>
//   );
// };

// export default ButtonAdd;

// const styles = StyleSheet.create({
//   addButton: {
//     backgroundColor: '#f25287',
//     width: 90,
//     height: 90,
//     borderRadius: 50,
//     alignSelf: 'flex-end',
//     alignItems: 'center',
//     justifyContent: 'center',
//     elevation: 8,
//     bottom: 20,
//     right: 20,
//   },
//   addButtonText: {
//     color: '#fff',
//     fontSize: 24,
//   },
// });
