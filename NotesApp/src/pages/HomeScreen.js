import React, {useEffect, useState} from 'react';
import Share from 'react-native-share';

import {useSelector} from 'react-redux';
import {useDispatch} from 'react-redux';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {request_data} from '../redux/action/notes';
import {request} from '../redux/action/request';
import Icon_Search from 'react-native-vector-icons/EvilIcons';
import Icon_Delete from 'react-native-vector-icons/AntDesign';
import Icon_Edit from 'react-native-vector-icons/Entypo';
import Icon_Share from 'react-native-vector-icons/AntDesign';

const HomeScreen = ({navigation}) => {
  const notes = useSelector(state => state.notes);
  const dispatch = useDispatch();

  useEffect(() => {
    //console.log('ini notes', notes);
  }, [notes]);

  const Item = ({item, index}) => {
    return (
      <View style={styles.item}>
        <View style={styles.item_title}>
          <View style={{width: '80%'}}>
            <Text style={styles.item_title_text}>{item.title}</Text>
          </View>
          <View style={styles.item_title_icon}>
            <TouchableOpacity onPress={() => DeleteRequest(item)}>
              <Icon_Delete name="delete" color="#9AD6F2" size={20} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => UpdateRequest(item.id)}>
              <Icon_Edit name="edit" color="#9AD6F2" size={20} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => ShareOpen(item)}>
              <Icon_Share name="sharealt" color="#9AD6F2" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <Text>{item.time}</Text>
        <View style={styles.item_pass_line}></View>
        <Text style={styles.item_description}>{item.description} </Text>
      </View>
    );
  };
  const DeleteRequest = itm => {
    const newItem = notes;
    const hapus = true;
    const none = false;
    const a = newItem.filter(dataFilter => {
      if (dataFilter !== itm) return dataFilter;
    });
    //console.log('ini adalah a', a);
    dispatch(request_data(a, hapus, none));
  };
  const UpdateRequest = itm => {
    //console.log('ini itm request', itm);
    dispatch(request(itm));
    navigation.navigate('EditNotes');
  };

  const ShareOpen = itm => {
    let title = itm.title;
    let des = itm.description;
    let date = itm.time;
    let a = `title : ${title} \ndescription : ${des} \nDate : ${date}`;
    const options = {
      title: 'bagikan melalui',
      message: a,
    };
    Share.open(options)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        err && console.log(err);
      });
  };
  const renderItem = ({item, index}) => {
    return <Item item={item} index={index} />;
  };

  return (
    <View style={{flex: 1, backgroundColor: '#EAEFF3'}}>
      <ScrollView style={{flex: 1}}>
        <View style={styles.header}>
          <View style={{marginHorizontal: 15, marginTop: 15}}>
            <Text style={{fontSize: 30}}>Hi Tami,</Text>
            <Text style={{fontSize: 20}}>Good Morning</Text>
          </View>
          <Image
            source={{
              uri: 'https://randomuser.me/api/portraits/women/17.jpg',
            }}
            style={styles.header_image}
          />
        </View>
        <View style={styles.search}>
          <Icon_Search name="search" color="#9AD6F2" size={40} />
          <TextInput style={styles.search_line} placeholder=""></TextInput>
        </View>
        <View style={styles.MyNotes}>
          <Text style={styles.MyNotes_text}>My Notes</Text>
        </View>
        <FlatList
          data={notes}
          renderItem={renderItem}
          keyExtractor={item => item.id}
          showsHorizontalScrollIndicator={false}
          numColumns={1}
        />
      </ScrollView>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  header_image: {
    width: 100,
    height: 100,
    marginRight: 30,
    marginTop: 15,
    borderRadius: 50,
    borderWidth: 4,
  },
  search: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
  },
  search_line: {
    marginVertical: 8,
    marginHorizontal: 15,
    justifyContent: 'flex-start',
    fontSize: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#9AD6F2',
    width: wp(80),
  },

  MyNotes: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    margin: 10,
  },
  MyNotes_text: {
    fontSize: 35,
    color: '#343437',
  },
  item: {
    flex: 1,
    backgroundColor: '#ffffff',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 10,
  },
  item_title: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  item_title_text: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  item_title_icon: {
    width: '20%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  item_description: {
    fontSize: 15,
  },
  item_pass_line: {
    justifyContent: 'flex-start',
    marginTop: 5,
    fontSize: 15,
    borderBottomWidth: 2,
    borderBottomColor: '#EAEFF3',
  },
});
