import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
} from 'react-native';
import {useDispatch} from 'react-redux';
import moment from 'moment';

import Icon_Save from 'react-native-vector-icons/AntDesign';
import {request_data} from '../redux/action/notes';

const AddNoteScreen = ({navigation}) => {
  const Time = moment(new Date()).format('DD MMMM YYYY, HH:mm:ss');
  const Time2 = moment(new Date()).format('DD MMMM YYYY');
  const Time3 = moment(new Date()).calendar();

  const idNumber = Math.random();
  const idString = String(idNumber);

  //console.log(request);

  const [time, setTime] = useState(Time);
  const [addNotes, setAddNotes] = useState({
    id: idString,
    title: '',
    description: '',
    time: time,
  });
  const [detailRequest, setDetailRequest] = useState({});

  useEffect(() => {
    setTime({...time, Time});

    //console.log('ini adalh e', tes);
  }, []);

  const dispatch = useDispatch();
  const AddNotes = data => {
    dispatch(request_data(data));
    navigation.navigate('Home');
    //console.log('ini adalah data new notes dari addNoteScreen', addNotes);
  };

  return (
    <View style={{flex: 1, backgroundColor: '#EAEFF3'}}>
      <View style={styles.container}>
        <View style={{flexDirection: 'column'}}>
          <Text
            style={{
              fontSize: 28,
              color: '#343437',
            }}>
            {Time2}
          </Text>
          <Text>{Time3}</Text>
        </View>
        <TouchableOpacity onPress={() => AddNotes(addNotes)}>
          <Icon_Save name="check" size={30} color="#339CD8" />
        </TouchableOpacity>
      </View>
      <View style={{marginTop: 50, paddingHorizontal: 15}}>
        <View>
          <Text>Title</Text>
          <TextInput
            style={{backgroundColor: '#ffffff', marginTop: 10}}
            onChangeText={changeTitle =>
              setAddNotes({...addNotes, title: changeTitle})
            }></TextInput>
        </View>
        <View style={{marginTop: 20}}>
          <Text>Description</Text>
          <TextInput
            style={{backgroundColor: '#ffffff', marginTop: 10, height: '60%'}}
            multiline={true}
            textAlignVertical="top"
            scrollEnabled={true}
            onChangeText={changeDes =>
              setAddNotes({...addNotes, description: changeDes})
            }></TextInput>
        </View>
      </View>
    </View>
  );
};

export default AddNoteScreen;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
    height: '10%',
    paddingHorizontal: 20,
  },
});
