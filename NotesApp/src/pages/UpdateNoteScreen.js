import React, {useEffect, useState} from 'react';
import ModalDropdown from 'react-native-modal-dropdown';
import {useDispatch} from 'react-redux';
import moment from 'moment';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
} from 'react-native';
import Icon_Back from 'react-native-vector-icons/Ionicons';
import Icon_Save from 'react-native-vector-icons/AntDesign';
import Icon_Option from 'react-native-vector-icons/SimpleLineIcons';
import {request_data} from '../redux/action/notes';
import {useSelector} from 'react-redux';

const UpdateNoteScreen = ({navigation}) => {
  const Time = moment(new Date()).format('DD MMMM YYYY, HH:mm:ss');
  const Time2 = moment(new Date()).format('DD MMMM YYYY');
  const Time3 = moment(new Date()).calendar();

  const notes = useSelector(state => state.notes);
  const request = useSelector(state => state.request);
  // console.log('ini notes ::::', notes);
  // console.log('ini request :::', request);
  const [timee, setTimee] = useState(Time);
  const [detailRequest, setDetailRequest] = useState({
    id: '',
    title: '',
    description: '',
    time: Time,
  });

  useEffect(() => {
    setDetailRequest({...detailRequest, time: Time});
  }, [detailRequest.id]);

  useEffect(() => {
    //console.log('halo', request);
    notes.forEach(element => {
      if (element.id.toLowerCase() === request.toLowerCase()) {
        setDetailRequest(element);
      }
    });
  }, [request]);

  const dispatch = useDispatch();
  const UpdateNotes = data => {
    console.log('ini data', data);
    const updateNewVersion = [];
    notes.forEach(el => {
      if (el.id.toLowerCase() === data.id.toLowerCase()) {
        updateNewVersion.push(data);
      } else {
        updateNewVersion.push(el);
      }
    });

    //console.log('ini update', updateNewVersion);
    dispatch(request_data(null, null, updateNewVersion));
    navigation.navigate('Home');
  };

  const dropDown = () => {
    return <ModalDropdown options={['option 1', 'option 2']} />;
  };

  return (
    <View style={{flex: 1, backgroundColor: '#EAEFF3'}}>
      <View style={styles.header}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon_Back name="chevron-back" size={30} color="#339CD8" />
          </TouchableOpacity>
          <View style={{flexDirection: 'column'}}>
            <Text
              style={{
                fontSize: 28,
                color: '#343437',
              }}>
              {Time2}
            </Text>
            <Text>{Time3}</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => UpdateNotes(detailRequest)}
            style={{padding: '2%'}}>
            <Icon_Save name="check" size={30} color="#339CD8" />
          </TouchableOpacity>
          {/* <TouchableOpacity onPress={() => dropDown()}>
            <Icon_Option name="options-vertical" size={20} color="#339CD8" />
          </TouchableOpacity> */}
        </View>
      </View>
      <View style={{marginTop: 50, paddingHorizontal: 15}}>
        <View>
          <Text>Title</Text>
          <TextInput
            value={detailRequest.title}
            style={{backgroundColor: '#ffffff', marginTop: 10}}
            onChangeText={changeTitle =>
              setDetailRequest({...detailRequest, title: changeTitle})
            }></TextInput>
        </View>
        <View style={{marginTop: 20}}>
          <Text>Description</Text>
          <TextInput
            value={detailRequest.description}
            style={{backgroundColor: '#ffffff', marginTop: 10, height: '60%'}}
            multiline={true}
            textAlignVertical="top"
            scrollEnabled={true}
            onChangeText={changeDes =>
              setDetailRequest({...detailRequest, description: changeDes})
            }></TextInput>
        </View>
      </View>
    </View>
  );
};

export default UpdateNoteScreen;

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
    height: '10%',
    paddingHorizontal: 0,
  },
});
