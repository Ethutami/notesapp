const initialState = {
  notes: [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: 'First Item',
      description: 'first Description',
      time: 'none',
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: 'Second Item',
      description: 'Second Description',
      time: 'none',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: 'Third Item',
      description: 'Thrid Description',
      time: 'none',
    },
  ],
  request: '',
};

const NoteReducer = (state = initialState, action) => {
  const {type, payload} = action;
  //console.log('data yang diterima reducer dari action', action);
  switch (type) {
    case 'ADD_DATA_REQUEST':
      return {
        ...state,
        notes: [...state.notes, payload], //{}
      };
    case 'DELETE_DATA_REQUEST':
      return {
        notes: payload,
      };
    case 'UPDATE_DATA_REQUEST':
      return {
        ...state,
        notes: payload,
      };
    case 'REQUEST_DATA':
      return {
        ...state,
        request: payload,
      };
    default:
      return state;
  }
};

export default NoteReducer;
