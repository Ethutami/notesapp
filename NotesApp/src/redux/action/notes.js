export const request_data = (data, hapus, update) => dispatch => {
  //console.log('helo ini adalah data', update);
  if (hapus) {
    return dispatch({
      type: 'DELETE_DATA_REQUEST',
      payload: data,
    });
  } else if (update) {
    return dispatch({
      type: 'UPDATE_DATA_REQUEST',
      payload: update,
    });
  }
  return dispatch({
    type: 'ADD_DATA_REQUEST',
    payload: data,
  });
};
