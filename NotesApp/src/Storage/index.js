import AsyncStorage from '@react-native-async-storage/async-storage';

export const storeData = async (key, value) => {
  //console.log(value);
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    // saving error
  }
  // console.log(getData(key).then(console.log));
  getData(key).then;
};

export const getData = async key => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    // error reading value
  }
};
